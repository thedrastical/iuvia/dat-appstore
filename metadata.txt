codename: "io.iuvia.market.dat"
version: "0.0.1"
metadata_version: 1
developer: "The Drastical Company"
l10n_default: "en"
license_code: "MIT"
license_text: "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions"
certificate: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDmKJ/4ZdyWYUxMp75r83RuUgtmE8EnP17lFhIlro1PY7yauzxGXWlfeI2OITeaQwwhNPQ/FLuCA6e0IOYfVyMxEXhxgM5Eoa1NDjkzpulufxTJCJakgWJbJYpmdZAX+sT6xB+pGiXZhT4tu+qFmjC+0uXUmkulX1NmQcVFS2eeve2X6B87xztvnLfmxHKwLbvz/R7y5psoqNJ3/ht4K7ST/5jQoyLoTAR0wFyJLBCXZt6BidW1fYPTagCBGH4nPWvZItBSZhRUNNW/mNhbfJGDct+mleQ3A4S4t6ONJyNpVJGtDE2+1Q6CaJvhfxOnIhNv+nKktBB4Yj3LKTkeh+OU+Xhww6D6KkiVUKHimh8TDj/CisGYAvDXYwTFf5IdRTWsr2M0fyVasyH1/6vo5p6OxilWbQ9s95T6g8PFQFEybfUHTAqlOKflZQd4FWNOwUpNOma3NMjVTWmk/hUrdWsdV6Gy/3as1+CmilE9iFPxcfHp3xynwtiOWHjaRODUcbE= appimage@iuvia.io"
icon_sizes: [128]
icon_locations: [
  {
    key: 128
    value: "helloworld.png"
  }
],
l10n: [
  {
    lang: "en"
    name: "IUVIA Marketplace"
    short_description: "IUVIA Marketplace"
    long_description: "This is the IUVIA P2P Marketplace using Dat."
  }
]
requires: [
  {
    codename: "BoxUI"
    min_version: "0.0.1"
    max_version: "0.~"
  }
]
expected_resources: {
  cpu: "some"
  ram: "1GB"
}
max_resources: {
  cpu: "lots"
  ram: "16GB"
}
launchers: [
  {
    platform: "self"
    on_activate: "iuvia:marketplace:0:unix"
  }
]
