'use strict';

var utils = require('../utils/writer.js');
var IPA = require('../service/IPAService');

module.exports.getIPA = function getIPA (req, res, next) {
  var codename = req.swagger.params['codename'].value;
  IPA.getIPA(codename)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
	utils.writeJson(res, response, response.error.status);
    });
};

module.exports.searchIPAs = function searchIPAs (req, res, next) {
  var q = req.swagger.params['q'].value;
  var hl = req.swagger.params['hl'].value;
  IPA.searchIPAs(q,hl)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
	utils.writeJson(res, response, response.error.status);
    });
};
