'use strict';

var utils = require('../utils/writer.js');
var Ipai = require('../service/IpaiService');

module.exports.getIPAVersionData = function getIPAVersionData (req, res, next) {
  var codename = req.swagger.params['codename'].value;
  var version = req.swagger.params['version'].value;
  Ipai.getIPAVersionData(codename,version)
	.then(function (response) {
	    res.writeHead(200, {'Content-Type': 'application/vnd.appimage'})
	    res.end(response)
    })
	.catch(function (response) {
	    console.log("GOT RESPONSE: ", JSON.stringify(response))
	    utils.writeJson(res, response, response.error.status);
	});
};

module.exports.getIPAVersionInfo = function getIPAVersionInfo (req, res, next) {
  var codename = req.swagger.params['codename'].value;
  var version = req.swagger.params['version'].value;
  if (version.match(/.AppImage$/)) {
      var version = req.swagger.params['version'].value = version.replace(/.AppImage$/i, '')
      return module.exports.getIPAVersionData(req, res, next)
  }
  Ipai.getIPAVersionInfo(codename,version)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
	utils.writeJson(res, response, response.error.status);
    });
};

module.exports.getIPAVersions = function getIPAVersions (req, res, next) {
  var codename = req.swagger.params['codename'].value;
  Ipai.getIPAVersions(codename)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
	utils.writeJson(res, response, response.error.status);
    });
};

module.exports.performIPAIAction = function performIPAIAction (req, res, next) {
  var codename = req.swagger.params['codename'].value;
  var version = req.swagger.params['version'].value;
  var action = req.swagger.params['action'].value;
  Ipai.performIPAIAction(codename,version,action)
    .then(function (response) {
      utils.writeJson(res, response);
    })
	.catch(function (response) {
	    console.log("MY RESPONSE IS THIS:", response)
	utils.writeJson(res, response, response.error.status);
    });
};
