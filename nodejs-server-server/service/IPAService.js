'use strict';

const { DatArchive } = require('dat-sdk/auto')

async function get_bundled_data() {
    const archive = await DatArchive.load('dat://mkt-dev.iss.iuvia.io')
    return JSON.parse(await archive.readFile('/bundled.json', {encoding: 'utf8'}))
}


/**
 * Retrieve IPA information by codename
 * Returns a Platform App
 *
 * codename String codename of the IPA to return
 * returns IPA
 **/
exports.getIPA = function(codename) {
    return new Promise(async function(resolve, reject) {
	var examples = {};
	examples['application/json'] = {
	    "codename" : "codename",
	    "last_available_version_release_date" : "2000-01-23T04:56:07.000+00:00",
	    "last_available_version" : "last_available_version",
	    "developer_key" : "developer_key",
	    "url" : "url"
	};
	try {
	    const bundled_data = await get_bundled_data()
	    const ipai = bundled_data.filter(f => f.content.codename == codename)[0]
	    ipai.last_available_version_release_date = "2020-02-20T00:00:00"
	    ipai.last_available_version = ipai.version
	    ipai.url = 'NOT_IMPLEMENTED'
	    resolve({
		'codename': ipai.content.codename,
		'last_available_version_release_date': '2020-02-20T00:00:00',
		'last_available_version': ipai.content.version,
		'url': '/ipa/' + ipai.content.codename,
		'_versions': '/ipa/' + ipai.content.codename + '/versions',
	    });
	} catch(e) {
	    reject({'error': {'status': 404}})
	}
    });
}


/**
 * Search for IUVIA Platform Apps available in this marketplace, by specifying criteria
 * 
 *
 * q String Text to search for (possibly internationalized) (optional)
 * hl List Language list to limit internationalization to (optional)
 * returns IPAList
 **/
exports.searchIPAs = function(q,hl) {
    return new Promise(async function(resolve, reject) {
	var examples = {};
	examples['application/json'] = {
	    "pages" : 1,
	    "_total_items" : 2,
	    "items" : [ {
		"codename" : "codename",
		"last_available_version_release_date" : "2000-01-23T04:56:07.000+00:00",
		"last_available_version" : "last_available_version",
		"developer_key" : "developer_key",
		"url" : "url"
	    }, {
		"codename" : "codename",
		"last_available_version_release_date" : "2000-01-23T04:56:07.000+00:00",
		"last_available_version" : "last_available_version",
		"developer_key" : "developer_key",
		"url" : "url"
	    } ]
	};
	try {
	    const bundled_data = await get_bundled_data()
	    resolve({
		'pages': 1,
		'_total_items': bundled_data.length,
		'items': bundled_data.map(f => f.content).map(f => {
		    return {
			'last_available_version_release_date': '2020-02-20T00:00:00',
			'last_available_version': f.version,
			'developer_key': f.developer,
			'url': '/ipa/' + f.codename,
			'codename': f.codename
		    }
		})
	    })
	} catch(e) {
	    console.log(e)
	    reject({'error': {'status': 500}, 'msg': e})
	}
    })
}

